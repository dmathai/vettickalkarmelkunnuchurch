function initGalleryMainPage(){
  $('.gallery').unbind().click(function(event){
   var galleryID = event.currentTarget.id;
   window.location.href = 'gallery/' + galleryID;
  });
};

function initMainPage(){
  if(window.location.href.indexOf('navto=') > 0){
    var navto = window.location.href.substr(window.location.href.indexOf('navto=') + 6, window.location.href.length);
    $('#main-wrapper').load('pages/'+ navto +'.html', function() {
      $('.active').removeClass('active');
      $('#'+navto).addClass('active');
      initGalleryMainPage();
    });
  } else {
    $('#main-wrapper').load('pages/home.html');
  }

  $('#mainNavbar a').unbind().click(function(event){
    $('.active').removeClass('active');
    var currentID = event.currentTarget.id;
    window.location.href = '/index.html?navto='+currentID;
    event.preventDefault();
  });
};

function initGallery(){
  $('.close-gallery').unbind().click(function(event){
    window.location.href = '../../index.html?navto=gallery';
  });
}

$(document).ready(function () {
  if(window.location.href.indexOf('/gallery/') > 0){
    initGallery();
  }else{
    initMainPage();
  }
});